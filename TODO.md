* ,init creates a configuration file in the user's home directory if it does
  not exist
* ,init in the cwd creates the conatiner for indexes and content
* tantivy indexes plain-text markup and YAML fields
* regexp extraction of facts from plain-text markup
* ,read either concatenates plain-text or stores binary data and associates it
  via content-hash.
* ,edit opens plain-text in configured $EDITOR
* ,anno makes notes about other notes so they are managed in the same way

# Timeline

Prototyping a "timeline" view.  Lots of unicode.  Lots of appropriate
whitespace.

02020 
  JAN
    1 ·⏺●◐◑
    2
    3
    4
    5
    ⁞
  FEB
    1
    2
    ⁞
  MAR

Some of the Fira glyphs are different...
↯

## Bullets/Boxes

• 2022
⦿ 29bf
⊚ 229a

There's gotta be a slightly faster way than picking out via Kitty's chooser...




