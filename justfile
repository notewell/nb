# build application
build: inst-clippy
    cargo check
    cargo clippy
    cargo test
    cargo build

# watch source tree for development changes for local testing
watch: inst-clippy inst-cargowatch
   cargo watch -x check -x clippy -x test -x 'build' -s 'cp target/debug/nb ~/bin/nb'

inst-clippy:
	command -v clippy || rustup component add clippy

inst-cargowatch:
  command -v cargo-watch || cargo install cargo-watch
