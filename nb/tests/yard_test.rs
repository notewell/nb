// extern crate we're testing, same as any other code would do.
extern crate nb;
use std::path::PathBuf;
#[test]
fn test_yard_lifecycle() {
    let mut cwd = PathBuf::new();
    cwd.push("/tmp/_testing_yard");
    let yard = nb::yard::Yard {
        name: String::from("testing yard"),
        path: cwd,
    };
    assert!(match yard.dispose() {
        Ok(_) => true,
        Err(_) => false,
    });
    assert_eq!(false, yard.exists());

    assert!(match yard.create() {
        Ok(_) => true,
        Err(_) => false,
    });
    assert_eq!(true, yard.exists());

    // creating again should succeed
    assert!(match yard.create() {
        Ok(_) => true,
        Err(_) => false,
    });
    assert!(match yard.num_items() {
        Ok(num) => num == 0,
        Err(_) => false,
    });
    for n in 1..=500 {
        let note = nb::notepoint::NotePoint::new(&format!("note_{} test", n), "Some content.");
        assert!(
            match nb::notepoint::NotePoint::create_in_yard(&note, &yard) {
                Ok(_) => true,
                Err(_) => false,
            }
        );
    }
    assert_eq!(500, yard.num_items().unwrap())
}
