extern crate bs58;
use blake3::Hasher;
use serde::{Serialize, Serializer};
use std::io::Read;

#[derive(Eq, PartialEq, Deserialize, Clone)]
pub struct ContentAddress {
    pub algorithm: String,
    #[serde(with = "serde_bytes")]
    pub address: Vec<u8>,
}

pub fn get_file_caddr(filepath: &str) -> Result<ContentAddress, std::io::Error> {
    let mut fh = std::fs::File::open(&filepath)?;
    let mut big_buf_blah = Vec::new();
    fh.read_to_end(&mut big_buf_blah)?;
    let mut bl3 = Hasher::new();
    bl3.update(&big_buf_blah);
    let hash = bl3.finalize();
    Ok(ContentAddress {
        algorithm: String::from("blake3"),
        address: hash.as_bytes().to_vec(),
    })
}
impl Serialize for ContentAddress {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let f = format!("{}", self);
        serializer.serialize_str(f.as_str())
    }
}

pub trait ContentAddressable {
    fn content_address(&self) -> ContentAddress;
}

use hex_slice::AsHex;
use std::fmt;
impl fmt::Display for ContentAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}/{}",
            self.algorithm,
            bs58::encode(&self.address).into_string()
        )
    }
}
impl fmt::Debug for ContentAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{:x}", self.algorithm, self.address.as_hex())
    }
}

impl ContentAddressable for String {
    fn content_address(&self) -> ContentAddress {
        let mut bl3 = Hasher::new();
        let mut vres: Vec<u8> = vec![];
        bl3.update(&self.as_bytes());
        let hash = bl3.finalize();
        vres.extend(hash.as_bytes().to_vec());
        ContentAddress {
            algorithm: String::from("blake3"),
            address: vres,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_content_address_of_string() {
        let s = String::from("Hello");
        assert_eq!(
            "blake3/HwmWvKKeKDLQdmeEWKUauq2RMwZMCBgPzvfid9M7nsFt",
            format!("{}", s.content_address())
        );
        let s = String::from("Hello world");
        assert_eq!(
            "blake3/GcFTh1ZUGFyhsc939fWegnECxp2H5opXNivAph7zruGQ",
            format!("{}", s.content_address())
        );
    }
    #[test]
    fn test_content_address_of_file() {
        match get_file_caddr("../sample.txt") {
            Ok(fhash) => {
                assert_eq!(
                    "blake3/4W89hjsGWJsQ5GYkr77wzyya9uXhpDscpY8tbpjbpKTS",
                    format!("{}", fhash)
                );
            }
            Err(msg) => assert_eq!(format!("{:?}", msg), "?"),
        }
    }
}
