use std::path::PathBuf;
use std::process::Stdio;

use clap::{App, Arg, SubCommand};

extern crate home;
use home::home_dir;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;

mod commands;
use crate::commands::*;

mod attitude;
mod caddr;
mod confidence;
mod note;
mod notepoint;
use notepoint::NotePoint;
mod point;
mod spacerep;
mod temporal;
mod waiting;
mod yard;

const APP_NAME: &str = "nb";
const APP_VERSION: &str = "0.2.0";

fn main() {
    let matches = App::new(APP_NAME)
        .version(APP_VERSION)
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Create).as_str())
                .about("create a new note")
                .arg(
                    Arg::with_name("TITLE")
                        .multiple(true)
                        .help("Sets the title of the note")
                        .required(true),
                )
                .arg(
                    Arg::with_name("file")
                        .short("f")
                        .long("file")
                        .multiple(false)
                        .help("Imports file and starts note's history with it")
                        .required(false)
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Init).as_str())
                .about("initialize a new yard"),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Read).as_str()).about("read a note"),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Delete).as_str())
                .about("delete a note"),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Update).as_str())
                .about("update a note")
                .arg(
                    Arg::with_name("ZTLID")
                        .help("ZTLID identifier to edit")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Mark).as_str())
                .about("mark a note for context")
                .subcommand(SubCommand::with_name("all").about("mark all notes in yard"))
                .subcommand(SubCommand::with_name("none").about("unmark all notes in yard")),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::ListYards).as_str())
                .about("list all known yards"),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Learn).as_str())
                .about("add note to learning system")
                .arg(
                    Arg::with_name("ZTLID")
                        .help("ZTLID of note to manage")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Learning).as_str())
                .about("show current status of training"),
        )
        .subcommand(
            SubCommand::with_name(Commands::prefixed(Commands::Review).as_str())
                .about("interactively review scheduled notes"),
        )
        .get_matches();

    let default_yard: yard::Yard;
    if let Some(home) = home_dir() {
        let mut yard_dir = home.clone();
        yard_dir.push(".nb_yard");
        default_yard = yard::Yard {
            name: String::from("default"),
            path: yard_dir,
        }
    } else {
        default_yard = yard::Yard {
            name: String::from("default"),
            path: PathBuf::from("."),
        }
    }

    if let Some(_) = matches.subcommand_matches(Commands::prefixed(Commands::Init).as_str()) {
        match default_yard.create() {
            Ok(_) => println!("Yard {:?} initialized", default_yard),
            Err(msg) => println!("Error: {:?}", msg),
        }
    }

    if let Some(matches) = matches.subcommand_matches(Commands::prefixed(Commands::Create).as_str())
    {
        let mut title = String::from("");
        let mut title_values = matches.values_of("TITLE").unwrap();
        while let Some(word) = title_values.next() {
            title.push_str(word);
            title.push_str(" ");
        }
        let ready_title = title.trim();
        let mut np = NotePoint::new(&ready_title, "");
        let opt_file = matches.values_of("file");
        match opt_file {
            None => {}
            Some(file) => {
                let mut file = file;
                if let Some(filepath) = file.next() {
                    let file_caddr = caddr::get_file_caddr(&filepath);
                    if let Ok(file_caddr) = file_caddr {
                        let ca = file_caddr.clone();
                        np.attachment = Some(ca);
                        let mut cwd = std::env::current_dir().unwrap();
                        cwd.push(filepath);
                        let dest = format!("{}/{}", default_yard.path.display(), &file_caddr);
                        match std::fs::copy(&cwd, &dest) {
                            Ok(_) => {
                                println!("Attached {}.", &filepath);
                            }
                            Err(e) => {
                                println!("Error attaching {}->{}: {:?}.", cwd.display(), dest, e);
                            }
                        }
                    }
                }
            }
        }
        match NotePoint::create_in_yard(&np, &default_yard) {
            Ok(_) => {
                println!("Created in {:?}", &default_yard.path);
            }
            Err(err) => {
                println!("Error: {:?}", err);
            }
        }
    }

    if let Some(edit) = matches.subcommand_matches(Commands::prefixed(Commands::Update).as_str()) {
        let editor = match std::env::var("EDITOR") {
            Err(_x) => String::from("/usr/local/bin/nvim"),
            Ok(editor_cmd) => editor_cmd,
        };
        let ztl = edit.value_of("ZTLID");
        match ztl {
            None => println!("Error finding ztl"),
            Some(z) => {
                let floc = format!("{}/{}.nb", &default_yard.path.to_str().unwrap(), z);
                println!("floc = {}", &floc);
                let mut out = std::process::Command::new(editor)
                    .args(&[floc])
                    .spawn()
                    .expect("failed to execute process");
                match out.wait() {
                    Err(x) => println!("Edit error: {}", x),
                    Ok(_) => println!("Editor exited."),
                }
            }
        };
    }
}
