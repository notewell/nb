use crate::attitude::AttitudeAdjustment;
use crate::temporal::Duration;
use crate::waiting::Waiting;
use std::fmt;

/// ConfidenceLevel is a logarithmic value that abstractly represents how "well" the user feels
/// about their understanding of an item.
pub struct ConfidenceLevel(AttitudeAdjustment);
pub trait Confidence {
    fn change(&mut self, amount: AttitudeAdjustment);
    fn raw(&self) -> f32;
}

impl Waiting for ConfidenceLevel {
    fn minutes(&self) -> Duration {
        Duration::new(self.0.exp().ceil() as u32)
    }
}
impl Confidence for ConfidenceLevel {
    fn change(&mut self, amount: AttitudeAdjustment) {
        let newcl = self.0 + amount;
        if newcl < 1.0 {
            self.0 = 1.0;
        } else {
            self.0 = newcl;
        }
    }
    fn raw(&self) -> f32 {
        self.0
    }
}
impl ConfidenceLevel {
    pub fn new() -> ConfidenceLevel {
        ConfidenceLevel(1.0)
    }
    pub fn change(cl: ConfidenceLevel, amount: f32) -> ConfidenceLevel {
        let newcl = cl.0 + amount;
        if newcl < 1.0 {
            ConfidenceLevel(1.0)
        } else {
            ConfidenceLevel(newcl)
        }
    }
    pub fn as_minutes(cl: &ConfidenceLevel) -> Duration {
        Duration::new(cl.0.exp().ceil() as u32)
    }
    pub fn as_seconds(&self) -> i64 {
        //! FIXME this is starting to smell code-wise.
        ConfidenceLevel::minutes(&self).as_seconds()
    }
}

impl fmt::Display for ConfidenceLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "cnf:{}", self.0)
    }
}

#[cfg(test)]
mod tests {
    use crate::confidence::*;
    #[test]
    fn can_reduce_confidence() {
        let c = ConfidenceLevel::new();
        let c = ConfidenceLevel::change(c, -7.0);
        assert!(c.raw() - 1.0 < std::f32::EPSILON);
    }

    #[test]
    fn can_add_confidence() {
        let c = ConfidenceLevel::new();
        let c = ConfidenceLevel::change(c, 0.6);
        assert!(c.raw() - 1.6 < std::f32::EPSILON);
    }

    #[test]
    fn can_represent_confidence() {
        let c = ConfidenceLevel::new();
        assert!(c.raw() - 1.0 < std::f32::EPSILON);
    }

    #[test]
    fn can_give_minutes_for_next_trial_from_confidence() {
        let c = ConfidenceLevel::new();
        let c = ConfidenceLevel::change(c, 0.6);
        let minutes = ConfidenceLevel::as_minutes(&c);
        assert_eq!(5, minutes.0);

        let c = ConfidenceLevel::new();
        let c = ConfidenceLevel::change(c, 4.6);
        let minutes = ConfidenceLevel::as_minutes(&c);
        assert_eq!(271, minutes.0);
    }
}
