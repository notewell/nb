//! # Hanke-Henry Permanent Calendar
//! Taken from: http://hankehenryontime.com/html/calendar.html
//!
//! The year is divided into four three-month quarters. Each month begins on the same day and date
//! each year. The first two months of each quarter are made up of 30 days, and the third is made
//! up of 31 days. Each quarter contains the same number of days, which simplifies financial
//! calculations; this methodology would have prevented Apple's Q4 2012 accounting fiasco, for
//! example.
//!
//! Each quarter contains 91 days, resulting in a 364-day year that is comprised of 52 seven-day
//! weeks. This is a vital feature of the HHPC because, by preserving the seven-day Sabbath cycle,
//! the HHPC abides by the fourth commandment, thereby avoiding the major complaints from
//! ecclesiastical quarters that have doomed all other attempts at calendar reform.
//!
//! Moreover, the HHPC accounts for the disparity between the length of our calendar (364 days) and
//! that of the astronomical calendar (roughly 365.24 days, the duration of one full orbit of the
//! Earth around the Sun) by simply tacking one additional full week to the end of every fifth or
//! sixth year (specifically 2020, 2026, 2032, 2037, 2043, 2048, and so on). This keeps the
//! calendar in line with the seasons, serving the same function as the leap year in the present
//! Gregorian system.
//!
//! The multiplicity of calendars can be explained by a variety of scientific, cultural, economic,
//! and religious factors. But, on a whole, it underscores the fact that no calendar has been able
//! to fully address all of the issues associated with measuring and organizing the passing of
//! time. The Hanke-Henry Permanent Calendar represents the most acceptable permanent calendar
//! reform to date.

pub enum Months {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
    Xtr,
}

pub fn days_in_month(mo: Months) -> u8 {
    match mo {
        Months::January => 30,
        Months::February => 30,
        Months::March => 31,

        Months::April => 30,
        Months::May => 30,
        Months::June => 31,

        Months::July => 30,
        Months::August => 30,
        Months::September => 31,

        Months::October => 30,
        Months::November => 30,
        Months::December => 31,

        Months::Xtr => 7,
    }
}

pub fn has_xtr_week(year: u32) -> bool {
    match year {
        2020 => true,
        2026 => true,
        2032 => true,
        2037 => true,
        2043 => true,
        2048 => true,
        // Get it working, then worry about an algo. :)
        _ => false,
    }
}
