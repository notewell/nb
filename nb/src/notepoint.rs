use crate::caddr::*;
use crate::note::Note;
use crate::point::Point;
use crate::yard::Yard;
use std::fs::File;
use std::io::{Error, ErrorKind, Write};
use std::path::Path;
extern crate serde;
use serde::Serialize;
extern crate ron;
use ron::ser::{to_string_pretty, PrettyConfig};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct NotePoint {
    pub note: Note,
    pub attachment: Option<ContentAddress>,
    pub point: Point,
}

impl NotePoint {
    pub fn new(title: &str, content: &str) -> NotePoint {
        let point = Point::new();
        let note = Note::new(title, content);
        let attachment = None;
        NotePoint {
            point,
            note,
            attachment,
        }
    }
    pub fn serialize(np: &NotePoint) -> Result<String, String> {
        let content = match &np.note.content {
            Some(x) => x.clone(),
            None => String::from(""),
        };
        // let content_hash = content.content_address();

        let pretty = PrettyConfig {
            depth_limit: 2,
            enumerate_arrays: true,
            separate_tuple_members: true,
            indentor: "  ".to_string(),
            new_line: "\n".to_string(),
        };
        let ronout = to_string_pretty(&np, pretty).expect("Serialization failed");
        let wrapped = format!("+++\n{}+++\n{}", ronout, content);
        Ok(wrapped)
    }
    pub fn create(np: NotePoint) -> Result<(), std::io::Error> {
        let filename = Point::filename(&np.point);
        let fpath = Path::new(&filename);
        if File::open(&fpath).is_ok() {
            let file_exists = Error::new(ErrorKind::Other, "file exists--won't overwrite");
            return Err(file_exists);
        };
        let mut file = match File::create(&fpath) {
            Err(why) => return Err(why),
            Ok(file) => file,
        };
        let ser_res = NotePoint::serialize(&np);
        match ser_res {
            Ok(ser) => file.write_all(ser.as_bytes()),
            Err(why) => {
                let ser_err = Error::new(ErrorKind::Other, format!("serialization error: {}", why));
                Err(ser_err)
            }
        }
    }
    pub fn attach_file(&mut self, filepath: &str, yd: &Yard) -> Result<(), Error> {
        let file_caddr = get_file_caddr(&filepath);
        match file_caddr {
            Ok(file_caddr) => {
                let mut cwd = std::path::PathBuf::new();
                cwd.push(filepath);
                let dest = format!("{}/{}", yd.path.display(), file_caddr);
                match std::fs::copy(&cwd, &dest) {
                    Ok(_) => {
                        println!("Attached {}.", &filepath);
                        self.attachment = Some(file_caddr);
                        Ok(())
                    }
                    Err(e) => {
                        println!("Error attaching {}->{}: {:?}.", cwd.display(), dest, e);
                        Err(e)
                    }
                }
            }

            Err(e) => Err(e),
        }
    }
    pub fn create_in_yard(np: &NotePoint, yd: &Yard) -> Result<(), std::io::Error> {
        let filename = Point::filename(&np.point);
        let mut ypath = yd.path.clone();
        let fpath = Path::new(&filename);
        ypath.push(fpath);
        if File::open(&ypath).is_ok() {
            let file_exists = Error::new(ErrorKind::Other, "file exists--won't overwrite");
            return Err(file_exists);
        };
        let mut file = match File::create(&ypath) {
            Err(why) => return Err(why),
            Ok(file) => file,
        };
        let yaml_res = NotePoint::serialize(&np);
        match yaml_res {
            Ok(yaml) => file.write_all(yaml.as_bytes()),
            Err(why) => {
                let ser_err = Error::new(ErrorKind::Other, format!("serialization error: {}", why));
                Err(ser_err)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn can_serialize_note() {
        use crate::notepoint::*;
        let np = NotePoint::new("title", "content");
        // let title_copy = np.note.title.clone().unwrap();
        // let content_copy = np.note.content.clone().unwrap();
        // let timestamp_copy = np.point.timestamp.clone();
        // let nonce_copy = np.point.nonce.clone();
        let res = NotePoint::serialize(&np);
        assert!(res.is_ok());
        if let Ok(_) = res {
            // Format not examined (changing too much)
        } else {
            panic!("Bad serialization");
        }
    }

    // #[test]
    // fn can_create_note() {
    //     use crate::notepoint::*;
    //     let np = NotePoint::new("title", "content");
    //     let res = NotePoint::create(np);
    //     assert!(res.is_ok());
    // }

    #[test]
    fn can_create_note_in_yard() {
        use crate::notepoint::*;
        use std::path::PathBuf;
        let mut yard_path = PathBuf::new();
        yard_path.push("/tmp/_testing_yard");
        let yard = Yard {
            name: "testing yard".to_string(),
            path: yard_path,
        };
        assert!(match yard.create() {
            Ok(_) => true,
            Err(x) => {
                // This has been a little hard to reproduce...
                if !x.to_string().contains("exists") {
                    println!("yard.create error: {}", x);
                    false
                } else {
                    true
                }
            }
        });
        let np = NotePoint::new("title", "content");
        let res = NotePoint::create_in_yard(&np, &yard);
        assert!(res.is_ok());
    }
    // #[test] disabled until I can track down the race. this is likely to change anyway...
    fn can_attach_file_in_yard() {
        use crate::notepoint::*;
        use std::path::PathBuf;
        let mut yard_path = PathBuf::new();
        yard_path.push("/tmp/_testing_yard");
        let yard = Yard {
            name: "testing yard".to_string(),
            path: yard_path,
        };
        assert!(match yard.create() {
            Ok(_) => true,
            Err(_) => false,
        });
        let mut np = NotePoint::new("title", "content");
        assert!(match np.attach_file("notfound.txt", &yard) {
            Err(_) => true,
            Ok(_) => false,
        });
        let mut cwd = std::env::current_dir().unwrap();
        cwd.push("../sample.txt");
        println!("infile={}", &cwd.display());
        println!("yard={:?}", &yard);
        assert!(
            match np.attach_file(&format!("{}", &cwd.display()), &yard) {
                Err(x) => {
                    println!("error: {:?}", x);
                    false
                }
                Ok(_) => true,
            }
        );
        let res = NotePoint::create_in_yard(&np, &yard);
        assert!(res.is_ok());
    }
}
