use chrono::prelude::*;
use std::fmt;
/// ZETTLE_TIMESTAMP is the standard "human-friendly" timestamp format.
/// This format sorts lexicographically.
pub const ZETTLE_TIMESTAMP: &str = "%Y%m%d.%H%M";

/// Points are the combination of a ZETTLE_TIMESTAMP with a random nonce string.  Many events
/// _could_ happen during a minute of time, it might be useful to guarantee the nonces also sort
/// lexicographically, but still be "random."
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Point {
    pub timestamp: String,
    pub nonce: String,
}

impl Point {
    fn new_timestamp() -> String {
        let utc: DateTime<Utc> = Utc::now();
        utc.format(ZETTLE_TIMESTAMP).to_string()
    }
    fn new_nonce() -> String {
        use rand::Rng;
        let mut rng = rand::thread_rng();
        let nonce: u64 = rng.gen();
        use std::mem::transmute;
        let bytes: [u8; 8] = unsafe { transmute(nonce.to_be()) };
        bs58::encode(bytes).into_string()
    }
    pub fn new() -> Point {
        let ts = Point::new_timestamp();
        let n = Point::new_nonce();
        Point {
            timestamp: ts,
            nonce: n,
        }
    }
    pub fn filename(pt: &Point) -> String {
        format!("{}-{}.nb", pt.timestamp, pt.nonce)
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", Point::filename(self))
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn can_make_todays_timestamp() {
        use crate::point::*;
        use chrono::prelude::*;
        let ts = Point::new_timestamp();
        let utc: DateTime<Utc> = Utc::now();
        assert_eq!(ts, utc.format(ZETTLE_TIMESTAMP).to_string());
    }

    #[test]
    fn can_make_a_nonce() {
        use crate::point::*;
        let n = Point::new_nonce();
        assert_ne!("", n);
        assert_ne!("fidelio", n);
    }

    #[test]
    fn can_fmt_a_filename_for_point() {
        use crate::point::*;
        let pt = Point::new();
        assert_eq!(
            format!("{}-{}.nb", pt.timestamp, pt.nonce),
            Point::filename(&pt)
        );
    }
}
