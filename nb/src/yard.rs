use std::fs;
use std::path::PathBuf;

use regex::Regex;

#[derive(Debug)]
pub struct Yard {
    pub name: String,
    pub path: PathBuf,
}

impl Yard {
    pub fn exists(&self) -> bool {
        let attr = fs::metadata(&self.path);
        match attr {
            Ok(meta) => meta.is_dir(),
            Err(_) => false,
        }
    }
    pub fn create(&self) -> Result<(), std::io::Error> {
        if !self.exists() {
            let mut khashpath = self.path.clone();
            khashpath.push("blake3");
            fs::create_dir(&self.path)?;
            fs::create_dir(&khashpath)
        } else {
            Ok(())
        }
    }
    pub fn dispose(&self) -> Result<(), std::io::Error> {
        if self.exists() {
            fs::remove_dir_all(&self.path)?;
        }
        Ok(())
    }

    pub fn index(&self) -> Result<(), ()> {
        Err(())
    }
    pub fn find(&self, _query: &str) -> Result<(), ()> {
        Err(())
    }
    fn is_path_a_note(path: &str) -> bool {
        lazy_static! {
            static ref RE: Regex = regex::Regex::new(r"\d{8}\.\d{4}-\w+\.nb$").unwrap();
        }
        return RE.is_match(path);
    }
    pub fn num_items(&self) -> std::io::Result<u64> {
        if self.exists() {
            let mut counted = 0;
            for entry in std::fs::read_dir(&self.path)? {
                let path = entry.unwrap().path();
                let name = path.to_str().unwrap();
                if Yard::is_path_a_note(name) {
                    counted += 1;
                }
            }
            return Ok(counted);
        }

        return Ok(0);
    }
}

#[cfg(test)]
mod tests {
    use crate::yard::*;
    #[test]
    fn can_recognize_a_note_by_filepath() {
        assert_eq!(false, Yard::is_path_a_note("asdf"));
        assert_eq!(false, Yard::is_path_a_note("20190913.1013-abcdefghijl"));
        assert_eq!(true, Yard::is_path_a_note("20190913.1013-abcdefghijl.nb"));
        assert_eq!(
            true,
            Yard::is_path_a_note("asdf/20190913.1013-abcdefghijl.nb")
        );
    }
}
