use std::fmt;

/// DurationCounter is a holder for some given unit of duration in minutes.
type DurationCounter = u32;
pub struct Duration(pub DurationCounter);
impl Duration {
    pub fn new(minutes: DurationCounter) -> Duration {
        Duration(minutes)
    }
}
impl Duration {
    pub fn scale(&mut self, multiple: u32) {
        self.0 *= multiple
    }
    /// Scale duration in minutes to seconds in the type Utc time usually has.
    pub fn as_seconds(&self) -> i64 {
        i64::from(self.0 * 60)
    }
    /// Scale duration in minutes to seconds in the type Utc time usually has.
    pub fn as_hours(&self) -> i64 {
        i64::from(self.0 / 60)
    }
    /// Scale duration in minutes to seconds in the type Utc time usually has.
    pub fn as_days(&self) -> i64 {
        self.as_hours() / 24
    }
    pub fn after(&self, timestamp: i64) -> i64 {
        self.as_seconds() + timestamp
    }
    pub fn before(&self, timestamp: i64) -> i64 {
        timestamp - self.as_seconds()
    }
}
impl fmt::Display for Duration {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", described_time(self))
    }
}

fn described_time(minutes: &Duration) -> String {
    // We're not being overly pedantic about matching up minutes
    // with days etc.

    let hour = 60;
    let day = hour * 24;
    let week = day * 7;
    let month = day * 30;
    let year = day * 365;

    let years;
    let months;
    let weeks;
    let days;
    let hours;
    let mut counted = minutes.0;
    let mut output = String::from("");
    if counted >= year {
        years = counted / year;
        counted -= years * year;
        output = format!("{}{}y", output, years);
    }
    if counted >= month {
        months = counted / month;
        counted -= months * month;
        output = format!("{}{}M", output, months);
    }
    if counted >= week {
        weeks = counted / week;
        counted -= weeks * week;
        output = format!("{}{}w", output, weeks);
    }
    if counted >= day {
        days = counted / day;
        counted -= days * day;
        output = format!("{}{}d", output, days);
    }
    if counted >= hour {
        hours = counted / hour;
        counted -= hours * hour;
        output = format!("{}{}h", output, hours);
    }
    if counted > 0 {
        output = format!("{}{}m", output, counted);
    }
    output
}

#[cfg(test)]
mod tests {
    use crate::temporal::Duration;
    #[test]
    fn can_display_human_interval_for_minutes() {
        let humanized = format!("{}", Duration(1));
        assert_eq!("1m", humanized);
        let humanized = format!("{}", Duration(59));
        assert_eq!("59m", humanized);
        let humanized = format!("{}", Duration(60));
        assert_eq!("1h", humanized);
        let humanized = format!("{}", Duration(61));
        assert_eq!("1h1m", humanized);
        let humanized = format!("{}", Duration(61_000));
        assert_eq!("1M1w5d8h40m", humanized);
        let humanized = format!("{}", Duration(610_000));
        assert_eq!("1y1M4w14h40m", humanized);
    }
    #[test]
    fn can_project_approximate_future_date() {
        use chrono::prelude::*;
        let a_day = Duration(24 * 60);
        let now = Utc::now().timestamp();
        let next_day_ts = now + a_day.as_seconds();
        assert_eq!(a_day.after(now), next_day_ts)
    }
    #[test]
    fn can_project_approximate_prior_date() {
        use chrono::prelude::*;
        let a_day = Duration(24 * 60);
        let day_30 = Duration(24 * 60 * 30);
        let now = Utc::now().timestamp();
        let month_from_now = now + a_day.as_seconds() * 30;
        assert_eq!(day_30.before(month_from_now), now);
    }
    #[test]
    fn can_derive_days_from_duration() {
        let day8 = Duration(24 * 60 * 8);
        assert_eq!(8, day8.as_days());
    }
    #[test]
    fn can_derive_hours_from_duration() {
        let day8 = Duration(24 * 60 * 8);
        assert_eq!(8 * 24, day8.as_hours());
    }
}
