#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Note {
    pub title: Option<String>,
    #[serde(skip_serializing)]
    pub content: Option<String>,
}

use crate::caddr::{ContentAddress, ContentAddressable};

impl ContentAddressable for Note {
    fn content_address(&self) -> ContentAddress {
        match &self.content {
            Some(content) => content.content_address(),
            None => match &self.title {
                Some(title) => title.content_address(),
                None => String::from("").content_address(),
            },
        }
    }
}

impl Note {
    pub fn new_simple(title: &str) -> Note {
        Note {
            title: Some(String::from(title)),
            content: None,
        }
    }
    pub fn new_scratch(content: &str) -> Note {
        Note {
            title: None,
            content: Some(String::from(content)),
        }
    }
    pub fn new(title: &str, content: &str) -> Note {
        Note {
            title: Some(String::from(title)),
            content: Some(String::from(content)),
        }
    }
}

// #[cfg(test)]
mod tests {
    use crate::note::*;

    #[test]
    fn simple_note() {
        let ttitle = "title";
        let sn = Note::new_simple(ttitle);
        assert_eq!(sn.title, Some(String::from(ttitle)))
    }

    #[test]
    fn new_scratch() {
        let content = "content";
        let sn = Note::new_scratch(content);
        assert_eq!(sn.title, None);
        assert_eq!(sn.content, Some(String::from(content)));
    }

    #[test]
    fn new_note() {
        let title = "title";
        let content = "content";
        let sn = Note::new(title, content);
        assert_eq!(sn.title, Some(String::from(title)));
        assert_eq!(sn.content, Some(String::from(content)));
    }
}
