
/// SlackLevel is the opposite of ConfidenceLevel and abstractly represents how much available
/// "procrastination" the user has for this item.  SlackLevels are based on the desired due date
/// for a decision.
pub struct SlackLevel(f32);

impl SlackLevel {
    pub fn new() -> SlackLevel {
        SlackLevel(1.0)
    }
    pub fn change(sl: SlackLevel, amount: f32) -> SlackLevel {
        let newsl = sl.0 + amount;
        if newsl < 1.0 {
            SlackLevel(1.0)
        } else {
            SlackLevel(newsl)
        }
    }
    pub fn minutes(sl: &SlackLevel) -> Duration {
        Duration::new(sl.0.exp().ceil() as u32)
    }
}
