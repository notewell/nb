use crate::temporal::Duration;
/// Waiting trait is for giving the remaining minutes before an activity is to
/// be started or resumed.
pub trait Waiting {
    fn minutes(&self) -> Duration;
}
