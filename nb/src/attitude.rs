/// AttitudeAdjustment makes it easier to change this type later if it
/// turns out to need some other Real numerical type.
pub type AttitudeAdjustment = f32;
