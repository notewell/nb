use crate::confidence::*;
use crate::point::Point;

use chrono::prelude::*;
use std::cmp::{Eq, PartialEq};
use std::collections::HashMap;
use std::fmt;
use std::hash::Hash;

/// Activity is either Learning or Ruminating, it affects scheduling.
pub enum Activity {
    // Learning activities start off frequent and decrease.
    Learning,
    // Ruminating activities start off as *infrequent* and decrease.
    Ruminating,
}

pub enum ActivityStatus {
    /// Pending state is for items that have not been decided upon for Learning|Ruminating
    Pending,
    /// For activites being performed,
    Active,
    /// For scheduling notes in advance
    Suspended,
    /// For items that depend on other items and should not be planned
    Blocked,
}

/// LearningEvaluation is a signal for how to adjust the confidence level of a note after a trial
/// where we ask the user how they think they did for the item. We base
/// on a five-item scale ranging from Unreviewed to Mastered.
#[derive(Eq, PartialEq, Hash)]
pub enum LearningEvaluation {
    /// Unreviewed items have never been studied before.
    Unreviewed,
    /// Forgotten items have been studied but they did not "stick".
    Forgotten,
    /// WeaklyRemembered items have been studied but are hazy "Oh yeah that thing!".
    WeaklyRemembered,
    /// Remembered is something studied and you remember it.
    Remembered,
    /// StronglyRemembered is something studied and you remember it fairly well.
    StronglyRemembered,
    /// Mastered is just obvious. If you forgot this, you would have your head examined.
    Mastered,
}
/// RuminatingEvaluation is a signal for how to adjust the confidence level of a note after a trial
/// where we ask the user how they think they did for the item.
#[derive(Eq, PartialEq, Hash)]
pub enum RuminatingEvaluation {
    /// Unreviewed items have never been studied before.
    Unreviewed,
    /// Unsure items have been considered but no possible decision would be made.
    Unsure,
    /// WeaklyDecided items have been studied but are ambivalent to some outcome.
    WeaklyDecided,
    /// Decided is something studied and the decision isn't final, but close.
    AlmostDecided,
    /// StronglyDecided is something that is very nearly a final decision.
    StronglyDecided,
    /// Decided is just that.
    Decided,
}

/// Study packages up a LearningEvaluation. [Is this even being used?]
pub struct Study(LearningEvaluation);

/// StudyEvaluationConfig is a mapping of study evaluation states to confidence changes.
pub type StudyEvaluationConfig = HashMap<LearningEvaluation, f32>;

/// A StudyEvent is the recording of when you evaluated a note.
pub struct StudyEvent {
    /// When occurred.
    timestamp: DateTime<Utc>,
    /// User's evaluation of their knowledge/understanding
    outcome: LearningEvaluation,
}

impl StudyEvent {
    fn new(outcome: LearningEvaluation) -> StudyEvent {
        let timestamp = Utc::now();
        StudyEvent { timestamp, outcome }
    }
}

/// The StudyState is an instrument for tracking and scheduling the use
/// of a point in a learning or decision system.
// #[derive(Serialize, Deserialize)] TODO impl: Deserialize for [caddrs and zettle timestamps]
pub struct StudyState {
    /// Pointer to thing to manage
    pub point: Point,
    /// Current confidence
    pub confidence: ConfidenceLevel,
    /// Approximate next review date
    pub scheduled: DateTime<Utc>,
    /// Learning history of point.
    pub history: Vec<StudyEvent>,
}

/// Studyable means this piece of data is managed for learning by a person.
trait Studyable {
    fn trial(&mut self, evaluation: Study, config: StudyEvaluationConfig);
}

impl Studyable for StudyState {
    fn trial(&mut self, evaluation: Study, config: StudyEvaluationConfig) {
        // Now why aren't I just grabbing the delta and changing the confidence
        // here? My gut is saying that I want the extra help from the compiler
        // to catch missing branches on the LearningEvaluation type.

        match evaluation {
            Study(u @ LearningEvaluation::Unreviewed) => {
                let delta = config[&u];
                self.confidence.change(delta);
            }
            Study(f @ LearningEvaluation::Forgotten) => {
                let delta = config[&f];
                self.confidence.change(delta);
            }
            Study(wr @ LearningEvaluation::WeaklyRemembered) => {
                let delta = config[&wr];
                self.confidence.change(delta);
            }
            Study(r @ LearningEvaluation::Remembered) => {
                let delta = config[&r];
                self.confidence.change(delta);
            }
            Study(sr @ LearningEvaluation::StronglyRemembered) => {
                let delta = config[&sr];
                self.confidence.change(delta);
            }
            Study(m @ LearningEvaluation::Mastered) => {
                let delta = config[&m];
                self.confidence.change(delta);
            }
        }
    }
}
impl fmt::Display for StudyState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "(study) |\n\t{} has confidence {};\n\tnext review in {};\n\toccurring after {}\n        |",
            self.point,
            self.confidence,
            ConfidenceLevel::as_minutes(&self.confidence),
            self.scheduled
        )
    }
}

impl StudyState {
    pub fn new(point: Point) -> StudyState {
        let time_now = Utc::now();
        StudyState {
            point,
            confidence: ConfidenceLevel::new(),
            scheduled: time_now,
            history: vec![StudyEvent {
                timestamp: time_now,
                outcome: LearningEvaluation::Unreviewed,
            }],
        }
    }
    pub fn trial(&mut self, event: &StudyEvent, config: StudyEvaluationConfig) {
        let time_now = Utc::now();

        self.confidence.change(config[&event.outcome]);
        self.scheduled = Utc.timestamp(time_now.timestamp() + self.confidence.as_seconds(), 0);
    }
}

/// configure_study sets up the default confidence changes for study evaluations.
pub fn configure_study() -> StudyEvaluationConfig {
    let mut study_evaluation_factors = HashMap::new();
    study_evaluation_factors.insert(LearningEvaluation::Unreviewed, 0.0);
    study_evaluation_factors.insert(LearningEvaluation::Forgotten, -1.3);
    study_evaluation_factors.insert(LearningEvaluation::WeaklyRemembered, -0.5);
    study_evaluation_factors.insert(LearningEvaluation::Remembered, 0.3);
    study_evaluation_factors.insert(LearningEvaluation::StronglyRemembered, 1.5);
    study_evaluation_factors.insert(LearningEvaluation::Mastered, 5.0);
    study_evaluation_factors
}

#[cfg(test)]
mod tests {
    use crate::spacerep::*;
    #[test]
    fn can_simulate_study_trials() {
        let pt = Point::new();
        let pt_copy = pt.clone();
        let mut sp = StudyState::new(pt);
        let time_now = Utc::now();
        let first_study_time = sp
            .scheduled
            .clone()
            .to_rfc3339_opts(SecondsFormat::Secs, true);
        let event = StudyEvent::new(LearningEvaluation::Forgotten);
        sp.trial(&event, configure_study());
        let expected = Utc
            .timestamp(time_now.timestamp() + sp.confidence.as_seconds(), 0)
            .to_rfc3339_opts(SecondsFormat::Secs, true);
        let next_study_time = sp
            .scheduled
            .clone()
            .to_rfc3339_opts(SecondsFormat::Secs, true);
        assert_eq!(next_study_time, expected);
    }
    fn can_create_study_point() {
        let pt = Point::new();
        let pt_copy = pt.clone();
        let sp = StudyState::new(pt);
        assert_eq!(pt_copy, sp.point);
    }

    #[test]
    fn can_display_study_point() {
        let pt = Point::new();
        let pt_copy = pt.clone();
        let sp = StudyState::new(pt);
        // I'm not going to care what this looks like yet but it should have
        // the point inside of it.
        assert!(format!("{}", sp).contains(&format!("{}", pt_copy)));
    }
}
