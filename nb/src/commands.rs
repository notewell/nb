/// Commmands from the command line, imagine that?
pub enum Commands {
    /// Create a new yard.  Yards are containers of notes and indexes. Indexes
    /// that are implicitly generated from the notes can be tossed. Explicit
    /// indexes relating the notes together are always kept.
    Init,
    /// Create a new note in the currently selected yard.
    Create,
    /// View a note.
    Read,
    /// Change the contents of a note.
    Update,
    /// Remove a note from the yard.
    Delete,
    /// Enumerate all the known yards.
    ListYards,
    /// Mark a note for collective action.
    Mark,
    /// Schedule a note for review.
    Learn,
    /// Train with the notes about to fade.
    Review,
    /// Show the notes scheduled for learning.
    Learning,
    /// Import media
    Import,
    /// Export media
    Export,
}
/// SUBCOMMAND_PREFIX is typed to differntiate a command from a find
pub const SUBCOMMAND_PREFIX: &str = ",";

impl Commands {
    pub fn name(c: Commands) -> &'static str {
        match c {
            Commands::Init => "init",
            Commands::ListYards => "yards",
            Commands::Create => "create",
            Commands::Read => "read",
            Commands::Update => "edit",
            Commands::Delete => "drop",
            Commands::Mark => "mark",
            Commands::Learn => "learn",
            Commands::Review => "review",
            Commands::Learning => "learning",
            Commands::Import => "import",
            Commands::Export => "export",
        }
    }

    /// alias commands are abbreviations. I should display these
    /// somehow from the help prompt. (TODO)
    pub fn alias(c: Commands) -> Option<&'static str> {
        match c {
            Commands::Create => Some("cr"),
            Commands::Read => Some("rd"),
            Commands::Update => Some("ed"),
            Commands::Delete => Some("dr"),
            Commands::Mark => Some("mk"),
            Commands::Learn => Some("lrn"),
            Commands::Review => Some("rev"),
            _ => None,
        }
    }
    pub fn prefixed(c: Commands) -> String {
        format!("{}{}", SUBCOMMAND_PREFIX, Commands::name(c))
    }
}
